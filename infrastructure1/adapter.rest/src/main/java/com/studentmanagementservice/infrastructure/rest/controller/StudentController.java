package com.studentmanagementservice.infrastructure.rest.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.studentmanagementservice.domain1.model.Student;




public interface StudentController {
	    @GetMapping("/student")
	    ResponseEntity<List<Student>> getStudents();

	    @PostMapping("/student")
	    ResponseEntity<Void> addStudent(@RequestBody Student student);

	    @DeleteMapping("/student")
	    ResponseEntity<Void> removeStudent(@RequestBody Student student);

	    @GetMapping("/student/{id}")
	    ResponseEntity<Student> getStudentById(@PathVariable Integer id);
}
