package com.studentmanagementservice.infrastructure1.adapter.graphql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;

import graphql.kickstart.tools.GraphQLQueryResolver;

@Component
public class Query implements GraphQLQueryResolver{
	private StudentRepository studentRepository;
	@Autowired 
	public Query(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	public Iterable<StudentEntity> findAllStudent(){
		return studentRepository.findAll();
	}
	
}
