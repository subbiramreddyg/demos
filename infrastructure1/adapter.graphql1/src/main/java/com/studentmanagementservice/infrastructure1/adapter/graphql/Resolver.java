package com.studentmanagementservice.infrastructure1.adapter.graphql;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;

import graphql.kickstart.tools.GraphQLResolver;

@Component
public class Resolver implements GraphQLResolver<StudentEntity>{
	private StudentRepository sRepo;
	
	@Autowired
	public Resolver(StudentRepository sRepo) {
		this.sRepo = sRepo;
	}
	public StudentEntity getStudent(int studentId) {
		return sRepo.getById(studentId);
	}

}
