package com.studentmanagementservice.infrastructure1.adapter.graphql;

import graphql.kickstart.tools.GraphQLMutationResolver;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;


@Component
public class Mutation implements GraphQLMutationResolver{
	private StudentRepository studentRepository;
	
	@Autowired
	public Mutation(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}
	public StudentEntity addStudent(String name,String qualification,String city) {
		
		StudentEntity student2=new StudentEntity();
		student2.setName(name);
		student2.setQualification(qualification);;
		student2.setCity(city);
		studentRepository.save(student2);
		
		return student2;
	}
	public String deleteStudent(int studentId) {
		studentRepository.deleteById(studentId);
		return "Student ID "+studentId+" deleted";
	}
	
	
}
