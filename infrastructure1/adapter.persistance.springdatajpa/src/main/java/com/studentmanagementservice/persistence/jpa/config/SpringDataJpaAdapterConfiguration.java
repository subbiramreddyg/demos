package com.studentmanagementservice.persistence.jpa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.studentmanagementservice.domain1.spi.StudentPersistencePort;
import com.studentmanagementservice.persistence.jpa.adapter.StudentSpringJpaAdapter;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;

@Configuration
public class SpringDataJpaAdapterConfiguration {
	 @Bean
	    public StudentPersistencePort getStudentPersistencPort(StudentRepository studentRepository) {
	        return new StudentSpringJpaAdapter(studentRepository);
	    }
}
