package com.studentmanagementservice.persistence.jpa.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.studentmanagementservice.domain1.model.Student;
import com.studentmanagementservice.domain1.spi.StudentPersistencePort;
import com.studentmanagementservice.persistence.jpa.entity.StudentEntity;
import com.studentmanagementservice.persistence.jpa.repository.StudentRepository;



public class StudentSpringJpaAdapter implements StudentPersistencePort {
	private StudentRepository studentRepository;

    public StudentSpringJpaAdapter(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

   
    public void addStudent(Student student) {
    	StudentEntity studentEntity = new StudentEntity();
        BeanUtils.copyProperties(student, studentEntity);
        studentRepository.save(studentEntity);
    }

    
    public void removeStudent(Student student) {
    	StudentEntity studentEntity = new StudentEntity();
        BeanUtils.copyProperties(student, studentEntity);
        studentRepository.delete(studentEntity);
    }


    public List<Student> getStudents() {
        List<Student> studentList = new ArrayList<Student>();
        List<StudentEntity> studentEntityList = studentRepository.findAll();
        for(StudentEntity studentEntity : studentEntityList) {
        	Student student = new Student();
            BeanUtils.copyProperties(studentEntity, student);
            studentList.add(student);
        }
        return studentList;
    }


    public Student getStudentById(int studentId) {
    	StudentEntity studentEntity = studentRepository.findByStudentId(studentId);
        if (studentEntity == null) {
            //throw new ProductNotFoundException
        }
        Student student = new Student();
        BeanUtils.copyProperties(studentEntity, student);

        return student;
    }


	

	

}
