package com.studentmanagementservice.domain1.spi;

import java.util.List;

import com.studentmanagementservice.domain1.model.Student;



public interface StudentPersistencePort {
	
	void addStudent(Student student);

    void removeStudent(Student student);

    List<Student> getStudents();

    Student getStudentById(int studentId);
	

}
